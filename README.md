# MDA-MB-231_Metabolic-Imaging

Data from Sunasse(2024) organized per mice.

Originally the images were black and white fluorescent microscope images representing 2-NBDb, Bodipy and TMRE. This dataset have the mixed images into an representative RGB. 

Methabolic-imaging-Dataset-1 : Mix 2-NBDG and Bodipy images considering they were aquired in completely different spectra. 2-NBDG is mixed mostly in the red spectra, and Bodipy is mostly in the green spectra.

Methabolic-imaging-Dataset-2 : Mix 2-NBDG and Bodipy  images considering they were aquired in the actual spectra, both in different "greens".

Methabolic-imaging-Dataset-2 : Mix  2-NBDG, Bodipy and TMRE images considering their real spectra.


[1] Sunassee, Enakshi D., et al. "Optical imaging reveals chemotherapy-induced metabolic reprogramming of residual disease and recurrence." Science Advances 10.14 (2024): eadj7540.
